# Raspberry Pi Power Board 
  
This is a handy addon to help run a Raspberry Pi (RPi) in remote locations.  
The board is designed to accept inputs from interchangeable 12V battery sources.  This means you can hotswap out one battery with a fresh one and keep the RPi running during the process. Or, you could have a 240V AC to 12V DC + a 12V backup battery running the RPi.
The Raspberry Pi hat can be mounted onto a RPi 2B+, 3 or 4. You can power on the RPi manually on/off via the power switch.  The onboard realtime clock, OLED display and power measurements can be accessed via the i2C bus. An external DS18B20 temperature sensor can be connected to the board. 
  
For more documentation visit [https://gitlab.com/parkview/Pollinator-Pi](https://gitlab.com/parkview/Pollinator-Pi)
  
## Hardware Features:
---------
#### Version 1.0 Features: 

* dual 12V inputs, with jumpered status LEDs
* INA219 voltage and current measurements available for each 12V power input
* 12V to 5V at up to 3A DC-DC converter
* power switch for the 5V supply to RPi
* non-switched 12V output. Could power a HDMI screen?
* switched (by RPi) 12V 1A output
* 5V piezo buzzer
* connector for an external DS18B20 temperature sensor
* onboard DS3231 Real Time Clock with battery backup
* 5 way switch for menu navigation
* i2C and 3.3V power pinheader, for SSD1306 OLED (or similar) display and M3 mounting hole
* M2.5 RPi mounting holes 
* 2 layer PCB 
  
#### RPi Used GPIO's:
  
|Name    |   Pin | GPIO|
|:--------:|:---:|:---:|
|`Buzzer`  |   33 |  13|
|`IR LED`  |   23 |  11|
|`DS18B20` |   07 |  04|
|`SW CNTR` |   35 |  19|
|`SW Right`|   37 |  26|
|`SW Up`   |   38 |  20|
|`SW Down` |   36 |  16| 
|`SW Left` |   40 |  21|
  
## Available Software 

![Gitlab URL](images/RPi_Power-Board_QR_URL.png){ align=right : style="height:100px;width:100px"}
#### Python:
* `Onsite-RPi` - Python 2.7 and shell script code repository  

![RPi Power Board V1.0](images/RPi_Power_Board.jpg){ align=left : style="height:169px;width:280px"}  
