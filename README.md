## Pollinator-Pi  
-----------  
  
This is a collection of hardware, software and CAD files that make up the Pollinator-Pi project. Which is a project setup to monitor and photograph an orchid flower every 0.8 seconds in the hope of seeing a pollinator going in and out of the flower, catching it in the act of pollinating it.

Warning, a lot of this was writing using Python 2.7.

A bit more about the projects journey: https://forum.swmakers.org/viewtopic.php?f=9&t=68 and on Gitlab:  https://gitlab.com/parkview/Pollinator-Pi
