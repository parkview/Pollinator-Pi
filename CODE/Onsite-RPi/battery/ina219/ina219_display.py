#!/usr/bin/python
from Subfact_ina219 import INA219
import time
import Adafruit_SSD1306
import Image
import ImageDraw
import ImageFont
import datetime

# initialse variables
DATADIR = "/home/pi/battery/ina219"
DATAFILE = DATADIR + "/battery_stats" + datetime.datetime.now().strftime("%Y-%m-%d_%H%M%S.csv")
DATETIME = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
RST = 24
top = 2
x = 0

# initialise the OLED display
font = ImageFont.load_default()
disp = Adafruit_SSD1306.SSD1306_128_32(rst=RST)

# Initialize library.
disp.begin()

# Clear display.
disp.clear()
disp.display()

# Create blank image for drawing.
# Make sure to create image with mode '1' for 1-bit color.
width = disp.width
height = disp.height
image = Image.new('1', (width, height))
# Get drawing object to draw on image.
draw = ImageDraw.Draw(image)

# read the current sensor
ina = INA219()
volts   = ina.getBusVoltage_V()
current = abs(ina.getCurrent_mA())
#power   = ina.getPower_mW()
power = abs(volts * current / 1000.0)

# display readings to console
#print "Shunt   : %.3f mV" % ina.getShuntVoltage_mV()
print "Volts   : %.3f V" % volts
print "Current : %.3f mA" % current
#print "Power   : %.3f W" % (volts * current / 1000.0)
print "Power   : %.3f W" % (power)
#print "---------------"
#print "Calc Power : %.3f mW" % (volts * current)
#print "Power diff : %.3f mW" % (power - (volts * current))

# now show the stats on the OLED Display
TEXT1 = "V: " + str(volts) + " V"
TEXT2 = "A: " + str(current) + " mA"
TEXT3 = "W: " + str(power) + " W"
draw.text((x, top),    TEXT1,  font=font, fill=255)
draw.text((x, top+10), TEXT2, font=font, fill=255)
draw.text((x, top+20), TEXT3, font=font, fill=255)
# Display image.
disp.image(image)
disp.display()

exit()
# now record the data into a csv file
file = open(DATAFILE, 'a')
DATA1 = str(DATETIME), str(volts), str(current)
DATA = ','.join(DATA1) + '\n'
file.write(DATA)
file.close()

