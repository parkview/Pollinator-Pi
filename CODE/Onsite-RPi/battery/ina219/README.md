subfact_pi_ina219
=================

Python library to read values from the INA219 sensor breakout board from Adafruit via i2c

From: https://github.com/scottjw/subfact_pi_ina219
Date: 2015-04-03

Modifed by:  https://forums.adafruit.com/viewtopic.php?f=8&t=36007&start=30#p344324
and me - power * 4
