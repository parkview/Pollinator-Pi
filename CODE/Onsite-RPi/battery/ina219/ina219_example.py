#!/usr/bin/python

from Subfact_ina219 import INA219

ina = INA219()
volts   = ina.getBusVoltage_V()
current = abs(ina.getCurrent_mA())
power   = abs(ina.getPower_mW())

#print "Shunt   : %.3f mV" % ina.getShuntVoltage_mV()
print "Volts   : %.3f V" % volts
print "Current : %.3f mA" % current
print "Power   : %.3f W" % (power / 1000.0)
#print "---------------"
#print "Calc Power : %.3f mW" % (volts * current)
#print "Power diff : %.3f mW" % (power - (volts * current))
