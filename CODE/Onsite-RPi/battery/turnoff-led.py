import wiringpi2
wiringpi2.wiringPiSetupGpio()   # set GPIO mode to GPIO pin numbers
wiringpi2.pinMode(18,1)         # set pin 25 as output
wiringpi2.digitalWrite(18,0)    # set pin 18 (LED flood light) to low
