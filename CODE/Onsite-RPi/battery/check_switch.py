'''
 this code will run auto run when battery-pi is turned on.  It detects when the start/stop switch is pressed and 
 notifies the main battery-pi code of the switch status

 Paul Hamilton: 2015-12-19

ToDo:
 
 *  needs switch debouce routines

'''

import wiringpi2

# local variables:
SWITCH = 9         # RPi switch pin
status_file = "/home/pi/battery/switch-status"


# Functions:

def write_status(status):
    fh = open(status_file,'w')
    fh.write(status) # python will convert \n to os.linesep
    fh.close()


wiringpi2.wiringPiSetupGpio()   # set GPIO mode to GPIO pin numbers
wiringpi2.pinMode(SWITCH,0)       # set pin 9 (switch) as input

while 1:
    switch_status = wiringpi2.digitalWrite(SWITCH)
    if switch_status == 1:
        write_status(load-on)
