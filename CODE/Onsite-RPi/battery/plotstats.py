'''

 this will plot any un plotted battery run stats files that it finds

  Parkview - plotstats.py - 2015-05-09



ToDo:

 * DONE: how to integrate with battery stats code?
 * DONE: dual axies
 * DONE: create appropriate plot name
 * DONE: create routine to find files that need graphing
 *

'''

import csv
import sys
import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt


# Local Variables
localdir = "/home/pi/battery"
batterystat_filenames = localdir + "/datafiles.txt"
graph_filenames = localdir + "/graphfiles.txt"


# Functions
def read_firstline(filename):
    with open(filename, 'rb') as fh:
        lines = fh.readlines()
        return lines

def fix_datetime(text):
    fixed_datetime = text.replace(':','').replace(' ', '_')
    #text1=fixed_datetime.split('_')
    #print text1[0]
    #textdate = text1[0].split('-')
    #fixdate = textdate[2] + "-" + textdate[1] + "-" + textdate[0]
    #print text, fixed_datetime
    #text1[0] = fixdate 
    return fixed_datetime

def generate_graph(filename):
    xData = []
    yData1 = []
    yData2 = []
    # go get the first line of text, so we can extract the datetime field
    firstline = read_firstline(filename)[0].split(',')
    plot_datetime = fix_datetime(firstline[0])   # clean up the datetime format
    # read in the battery stats csv file data:
    f = open(filename, 'r') #open file
    try:
        r = csv.reader(f) #init csv reader
        for row in r:
            #assuming we have the format -  datetime, count, volts, current, cpu temp
            xData.append(row[1])
            yData1.append(row[2])
            yData2.append(row[3])
            #print row[1], row[2], row[3]
    finally:
        f.close() #cleanup

    # generate graphs
    fig,v = plt.subplots()
    fig.set_size_inches(12, 6)
    plt.title('Battery Run-Time Data: ' + plot_datetime, size=14)
    a = v.twinx()
    v.set
    v.set_xlabel('Time (minutes)', size=14)
    v.set_ylabel('Battery Voltage (V)', color='b', size=14)
    a.set_ylabel('Battery Current (mA)', color='r', size=14)
    for tl in v.get_yticklabels():     # set color to Y axis 1 ticks
        tl.set_color('b')
    for tl in a.get_yticklabels():     # set color to Y axis 2 ticks
        tl.set_color('r')
    v.plot(xData, yData1, color='b', linestyle='-', marker='o')
    a.plot(xData, yData2, color='r', linestyle='-')
    fig.savefig('graphs/batterydata_' + plot_datetime + '.jpg', bbox_inches='tight', format='jpg')


# Main program:
with open(graph_filenames, 'rb') as fh:   # read lines from master battery data file
    glines = fh.readlines()
fh.close
graph_lines = set(glines)
#print "Graph List: ",graph_lines

with open(batterystat_filenames, 'rb') as fh:  # read in lines from master graph file list
    battery_lines = fh.readlines()
fh.close
#print "Battery List: ",battery_lines

for line in battery_lines[:-1]:  # check all items, but the last one (it's still being written too)
    # check to see if the corrosponding line/file exists in the graph file
    print "battery line: ",line.strip()
    if line not in graph_lines:
        # file hasn't been graphed, so go do it
        generate_graph("stats/" + line.strip())
        print "Generating graph for: ", line.strip(), "\n"
        # save file name into master graph filename data file
        gfh = open(graph_filenames,'a')
        gfh.write(line) 
        gfh.close()
    else:
        print "battery  file has been graphed!\n" 

