#!/usr/bin/python
""" this will keep track of the currently battery runtime stats and create a graph once a new battery is swapped in.
    It's expected that this script will be called via a cron script once every 1 minute.

  Usage:  battery_stats.py

  Parkview - 2015-12-19

 To-Do's:

 * DONE: read battery stats and log to csv file
 * DONE: read battery stats file and compare last reading to current reading - if current is much larger (nn??), then start a new battery log, as it's most probably a new battery.  Graph the old battery data.
 * DONE: things to record:  datetime, count, voltage, current draw, cpu temperature
 * DONE: record new battery initial datetime to a master logfile, so we know what the current logfile name is
 * DONE: read masterfile to find current log file to read/check against
 * DONE: set script running via pi crontab
 *  modify code to read battery multiple times per minute and avg out readings
 *  blink red status LED - so user knows it's all working ok
 *  turn off load as needed (when is the battery flat?)


"""

# set module imports
import os, sys
from ina219.Subfact_ina219 import INA219
from datetime import datetime, timedelta
import wiringpi2
import Adafruit_SSD1306
import Image
import ImageDraw
import ImageFont
#import os.path


# setup local variables
interval = 1                                  # how many minutes between readings (should be run every 5 minutes)
localdir = "/home/pi/battery"                 # project directory
masterfile = localdir + "/datafiles.txt"      # contains a list of data file names
statsdir = "stats"   #  holds the battery stats files
graphdir = "graphs"  # holds the graphs of the last battery discharge run
flagfile = "/tmp/batterypi.run"               # flag file that controls f Battery-Pi runs or not
count = 0            # a count of how many times a battery have been in use for
fudge = 0.3          # the amount that a battery might rise by after it's been turned off for a while
readings = 500	     # number of readings per session.  This is a count down timer
DATADIR = "/home/pi/battery/ina219"
DATAFILE = DATADIR + "/battery_stats" + datetime.now().strftime("%Y-%m-%d_%H%M%S.csv")
DATETIME = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
RST = 24             # OLED Reset address
top = 2              # OLED display top
x = 0                # OLED display left hand side
volts_low = 10.65    # battery test cut off.  Turn off the battery load when we get to this voltage
LED = 15             # LED RPi output pin
LOAD = 10            # Resistive Load RPi output pin
# initialise the OLED display
font = ImageFont.load_default()
disp = Adafruit_SSD1306.SSD1306_128_32(rst=RST)
disp.begin()
width = disp.width
height = disp.height


# functions:

def clear_display():
    '''Clear OLED Display'''
    # Clear display.
    disp.clear()
    disp.display()


def write_display(line1, line2, line3):
    '''Write out three lines of data to the dx.com clone/Adafruit i2c OLED Display'''
    image = Image.new('1', (width, height))
    draw = ImageDraw.Draw(image)  # Get drawing object to draw on image.
    # now show the stats on the OLED Display
    TEXT1 = "V: %.2f V" % line1
    TEXT2 = "A: %d mA" % line2
    TEXT3 = "W: %.1f mW" % line3
    draw.text((x, top),    TEXT1,  font=font, fill=255)
    draw.text((x, top+10), TEXT2, font=font, fill=255)
    draw.text((x, top+20), TEXT3, font=font, fill=255)
    # Display image.
    disp.image(image)
    disp.display()

def write_text(line1, line2, line3):
    '''Write out three lines of text to the dx.com clone/Adafruit i2c OLED Display'''
    image = Image.new('1', (width, height))
    draw = ImageDraw.Draw(image)  # Get drawing object to draw on image.
    # now show the stats on the OLED Display
    TEXT1 =  line1
    TEXT2 =  line2
    TEXT3 =  line3
    draw.text((x, top),    TEXT1,  font=font, fill=255)
    draw.text((x, top+10), TEXT2, font=font, fill=255)
    draw.text((x, top+20), TEXT3, font=font, fill=255)
    # Display image.
    disp.image(image)
    disp.display()


def turnoffload():
    '''turns off the battery load switch MOSFET'''
    wiringpi2.digitalWrite(LOAD,0)    # set pin 10 (Battery MOSFet off) to low
    led_off()                         # turn off RED status LED
    os.remove(flagfile)               # remove flag file in the temp dir.  this will stop it from running again


def battery_current():
    '''Go get the current battery power usage'''
    ina = INA219()
    return ina.getCurrent_mA()


def battery_voltage():
    '''Go get the current battery Voltage usage'''
    ina = INA219()
    return ina.getBusVoltage_V()


def cpu_temperature():
    '''Go read the CPU temperature and return a decimal value'''
    temp1 = open('/sys/class/thermal/thermal_zone0/temp', 'r')
    cpu_temp = temp1.read()
    temp1.close()
    return float(cpu_temp) * 0.001


def read_firstline(filename):
     ''' returns the first line of the file'''
     fh = open(filename,"r")
     lines = fh.readlines()
     return lines[0]


def read_lastline(filename):
     ''' returns the last line of the file'''
     fh = open(filename,"r")
     lines = fh.readlines()
     file_length = len(lines)
     return lines[file_length - 1]


def blink_led():
     '''Toggle Red LED status.  Acts as a visual indicator that the circuit is working'''
     led_status = wiringpi2.digitalRead(LED)
     if led_status == 1:
         wiringpi2.digitalWrite(LED,0)    # set toggle LED pin
     else:
         wiringpi2.digitalWrite(LED,1)    # set toggle LED pin


def led_on():
     '''force the status LED on'''
     wiringpi2.digitalWrite(LED,1)    # set status LED pin to on


def led_off():
     '''force the status LED off'''
     wiringpi2.digitalWrite(LED,0)    # set status LED pin to off


def my_ipaddress():
    ''' fetch the current IP Address of eth0'''
    f = os.popen('ifconfig eth0 | grep "inet\ addr" | cut -d: -f2 | cut -d" " -f1')
    return f.read()


# main program

wiringpi2.wiringPiSetupGpio()   # set GPIO mode to GPIO pin numbers
wiringpi2.pinMode(LOAD,1)       # set pin 10 (battery load) as output
wiringpi2.pinMode(LED,1)        # set pin 15 (led) as output
# test to see if it's ok to run the program
if os.path.exists(flagfile):
    # flag file exists, so go ahead and runt he program
    clear_display()
    total_volt = 0.0
    total_current = 0.0
    total_watt = 0.0
    print  " "  # leave a blank line
    # run this loop for 'readings' number of times and work out the averages
    for loop_count in range(0,readings):
        volts = battery_voltage()          # Volts
        current = abs(battery_current())   # milliAmps
        watts = volts * current
        #cpu_temp = cpu_temperature()
        # clear_display()
        write_display(volts, current, watts)
        total_volt = total_volt + volts
        total_current = total_current + current
        total_watt = total_watt + watts    # milliWatts
        # print loop_count, volts, current, watts
        blink_led()
    # now work out the average readings for the last minute or so
    volts = total_volt / readings
    current = total_current / readings
    watt = total_watt / readings
    print " "
    print "Avg:    ", volts, current, watt

    # get current formatted datetime
    current_time = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    log_time = current_time.replace(':','').replace(' ', '_')


    # check to see what files exist:

    # should put these next three tests into a function...
    if os.path.isfile(masterfile):
        # if exists, go get the last logfile name from the file.  This becomes the: current_log
        #print "masterfile exists!"
        current_log = read_lastline(masterfile).strip()
        filestat = os.stat(statsdir + "/" + current_log)
        line = read_lastline(statsdir + "/" + current_log).strip()
        #print "**"+current_log+"**"
        data = line.split(',')
        #print "Data: ",data
        count = int(data[1].strip()) + 1
        lastvolt = float(data[2].strip())
    else:
        # if master file does't exist, then create it
        print "masterfile DOES NOT exist!"
        # will need to create the mastefile and then exit the script & insert filename.  It should run ok next cycle.
        current_log = "batterystats_" + log_time + ".txt"
        lastvolt = 0.0

    # check to see if the stored volt is much less than the current volt.
    if (lastvolt + fudge) < volts:
        # yes, this is a new battery, we have to:
        #  1) create a datafile name and creat the file
        #  2) store the filename in the datafiles.txt file
        #  3) reset the count back to zero
        #  4) to be done later:  create battery graph of the previous battery
        current_log = "batterystats_" + log_time + ".txt"
        # write line to text file:
        print "New battery!  Create new logfile..."
        fh = open(masterfile,'a')
        fh.write(current_log + "\n") # python will convert \n to os.linesep
        fh.close()
        # create a new blank file.  This could somehow be incorporated into the current_log file test???
        fh = open(statsdir + "/" + current_log,'a')
        fh.close() 
        count = 0
    else:
        # all ok, we are still using the same battery. 
        print "same battery, moving on..."

    text = current_time + "," + str(count) + "," + str(volts) + "," + str(current) + "," + str(watt) + "\n"

    if os.path.isfile(statsdir + "/" + current_log):
        # if current file exists, then read from it, otherwise error?????
        # write stats line to text file:
        fh = open(statsdir + "/" + current_log,'a')
        fh.write(text) # python will convert \n to os.linesep
        fh.close()
    else:
        # whoops, where did the previous log file go to??
        print "ERROR: Current log file:  **", current_log, "**NOT found!"
        exit()

    print "Current Data: ", text

    # test to see if the battery-pi rig needs to turn off the battery load, ie: shut down the battery test
    if volts < volts_low:
        print "Battery voltage is low:  Turning off battery load test!"
        # ok, time to turn off the battery load
        turnoffload()
    else:
        print "Battery voltage is ok!"
    led_on()
else:
    # flag file doesn't exist, so close down for this run
    print "skip run"
    led_off()                # turn off status LED
    #blink_led()
    volts = battery_voltage()
    current_time = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    write_text(current_time, "Volts:  " + str(volts), "IP:  " + my_ipaddress())

