import wiringpi2
wiringpi2.wiringPiSetupGpio()   # set GPIO mode to GPIO pin numbers
wiringpi2.pinMode(10,1)         # set pin 10 (battery) as output
wiringpi2.digitalWrite(10,0)    # set pin 10 (Battery MOSFet off) to low
