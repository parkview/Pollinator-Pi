#!/bin/sh
#
# This script is called =via /etc/rc.local  and kicks off Battery-Pi
#
#  Paul hamilton - 2016-01-09
#

cd /home/pi/battery
sudo python /home/pi/battery/boot_up.py 
