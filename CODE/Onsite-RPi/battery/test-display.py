import time
     
import Adafruit_SSD1306
     
import Image
import ImageDraw
import ImageFont
RST = 24
top = 2
x = 0

font = ImageFont.load_default()

disp = Adafruit_SSD1306.SSD1306_128_32(rst=RST)

# Initialize library.
disp.begin()
 
# Clear display.
disp.clear()
disp.display()
 
# Create blank image for drawing.
# Make sure to create image with mode '1' for 1-bit color.
width = disp.width
height = disp.height
image = Image.new('1', (width, height))
 
print width, height

# Load default font.
font = ImageFont.load_default()

# Get drawing object to draw on image.
draw = ImageDraw.Draw(image)


# Write two lines of text.
draw.text((x, top),    'Hello',  font=font, fill=255)
draw.text((x, top+10), 'World!', font=font, fill=255)

# Display image.
disp.image(image)
disp.display()

