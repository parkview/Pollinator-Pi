#!/usr/bin/python
""" This is meant to be auto run on power up of Battery-Pi.  It just checks to see if the battery run switch is pressed.

    Paul Hamilton 2016-01-06

"""



import os, sys
from datetime import datetime, timedelta
import wiringpi2
import Adafruit_SSD1306
import Image
import ImageDraw
import ImageFont
from time import sleep


flagfile = "/tmp/batterypi.run"               # flag file that controls f Battery-Pi runs or not
LED = 15             # LED RPi output pin
LOAD = 10            # Resistive Load RPi output pin
SWITCH = 9           # Battery-Pi load on/off switch



# initialise the OLED display
font = ImageFont.load_default()
RST = 24             # OLED Reset address
disp = Adafruit_SSD1306.SSD1306_128_32(rst=RST)
disp.begin()
width = disp.width
height = disp.height
top = 2              # OLED display top
x = 0                # OLED display left hand side
switch_status = 0    # keeps track of the battery load switch status
load_status = 0      # track status if the load is on/off


# functions:

def clear_display():
    '''Clear OLED Display'''
    disp.clear()
    disp.display()


def write_text(line1, line2, line3):
    '''Write out three lines of text to the dx.com clone/Adafruit i2c OLED Display'''
    image = Image.new('1', (width, height))
    draw = ImageDraw.Draw(image)  # Get drawing object to draw on image.
    # now show the stats on the OLED Display
    TEXT1 =  line1
    TEXT2 =  line2
    TEXT3 =  line3
    draw.text((x, top),    TEXT1,  font=font, fill=255)
    draw.text((x, top+10), TEXT2, font=font, fill=255)
    draw.text((x, top+20), TEXT3, font=font, fill=255)
    # Display image.
    disp.image(image)
    disp.display()


def led_off():
     '''force the status LED off'''
     wiringpi2.digitalWrite(LED,0)    # set status LED pin to off


def turnoffload():
    '''turns off the battery load switch MOSFET'''
    wiringpi2.digitalWrite(LOAD,0)    # set pin 10 (Battery MOSFet off) to low
    led_off()                         # turn off RED status LED
    flag_remove(flagfile)


def flag_remove(path):
    ''' remove the flag file if it exists'''
    if os.path.exists(path):
        try:
            os.remove(path)           # remove flag file in the temp dir.  this will stop it from running again
        except OSError, e:  ## if failed, report it back to the user ##
            print ("Error: %s - %s." % (e.filename,e.strerror))


def flag_add(path):
    ''' create the flag file'''
    with open(path, 'a'):
        os.utime(path, None)


#  Main Program:
wiringpi2.wiringPiSetupGpio()   # set GPIO mode to GPIO pin numbers
wiringpi2.pinMode(LOAD,1)       # set pin 10 (battery load) as output
wiringpi2.pinMode(LED,1)        # set pin 15 (led) as output
wiringpi2.pinMode(SWITCH,0)     # set pin 9 (switch) as input
turnoffload()                   # remove flag file in the temp dir.  Just making sure the program doesn't auto start
clear_display()
DATETIME = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
write_text("     SHUT DOWN!", " ", DATETIME)
