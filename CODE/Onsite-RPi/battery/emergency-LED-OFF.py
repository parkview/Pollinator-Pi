#  this will turn off the DOC-Pi LED flood light


import wiringpi2
wiringpi2.wiringPiSetupGpio()   # set GPIO mode to GPIO pin numbers
wiringpi2.pinMode(18,1)         # set pin 18 as output
wiringpi2.digitalWrite(18,0)    # output a low on pin 18
