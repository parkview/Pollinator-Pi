#!/bin/sh
#
# this script just removes a blocking flag file, which then allows the flower-pi.sh cron started script to start running.
# See the corresponding 'stop_flowerpi.sh' script that installs the blocking script and then kills all the processes
#
# Also see the 'kill_flower-pi.sh' script that just kills all current python processes (I hope the temperature python script isn't running!
#
#
#  Paul Hamilton 2016-08-13

/bin/rm /tmp/block_flowerpi
