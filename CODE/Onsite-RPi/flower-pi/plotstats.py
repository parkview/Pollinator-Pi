'''

 this will plot any un plotted battery run stats files that it finds

  Parkview - plotstats.py:  2015-05-09
  Updated for flower-pi use:  2016-08-06



ToDo:

 * DONE: how to integrate with battery stats code?
 * DONE: dual axies
 * DONE: create appropriate plot name
 * DONE: create routine to find files that need graphing
 *

'''

import csv
import sys
import datetime as dt
import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
from matplotlib.dates import DateFormatter


# Local Variables
localdir = "/home/pi/flower-pi"     # home difrectory from this project
batterystat_filenames = localdir + "/datafiles_temperature.txt"    # list of all possible data files that either have or haevn't been graphed before
graph_filenames = localdir + "/graphfiles.txt"      # file containing graph file names, ie: list of previous graphs that have been processed


# Functions
def read_firstline(filename):
    ''' reads the first line of a file '''
    with open(filename, 'rb') as fh:
        lines = fh.readlines()
        return lines

def fix_datetime(text):
    ''' fix up date format syntax issues.  NOTE: this might be able to be fixed using datetime formatting string? '''
    fixed_datetime = text.replace(':','').replace(' ', '_')
    return fixed_datetime

def generate_battery_graph(filename):
    ''' generates a graph from the passed in data file'''
    xData = []
    yData1 = []
    yData2 = []
    # go get the first line of text, so we can extract the datetime field
    firstline = read_firstline(filename)[0].split(',')
    plot_datetime = fix_datetime(firstline[0])   # clean up the datetime format
    # read in the battery stats csv file data:
    f = open(filename, 'r') #open file
    try:
        r = csv.reader(f) #init csv reader
        for row in r:
            #assuming we have the format -  datetime, volts, current, watts, air temp, cpu temp, file space %
            xData.append(row[0])
            yData1.append(row[1])
            yData2.append(row[2])
            #print row[0], row[1], row[2]
    finally:
        f.close() #cleanup

    # adjust/cleanup xdata/Datetime data
    xData = [dt.datetime.strptime(d,'%Y-%m-%d_%H%M%S.%f') for d in xData]
    # generate graphs
    fig,v = plt.subplots()
    fig.set_size_inches(12, 6)
    plt.title('Battery Run-Time Data: ' + plot_datetime, size=14)
    a = v.twinx()
    v.set
    v.set_xlabel('Time (minutes)', size=14)
    v.set_ylabel('Battery Voltage (V)', color='b', size=14)
    a.set_ylabel('Battery Current (mA)', color='r', size=14)
    for tl in v.get_yticklabels():     # set color to Y axis 1 ticks
        tl.set_color('b')
    for tl in a.get_yticklabels():     # set color to Y axis 2 ticks
        tl.set_color('r')
    formatter = DateFormatter('%d %H:%M')
    plt.gcf().axes[0].xaxis.set_major_formatter(formatter)
    v.plot(xData, yData1, color='b', linestyle='-')
    a.plot(xData, yData2, color='r', linestyle='-')
    fig.savefig('graphs/' + plot_datetime + '_battery.jpg', bbox_inches='tight', format='jpg')


def generate_temperature_graph(filename):
    ''' generates a graph from the passed in data file'''
    xData = []
    yData1 = []
    yData2 = []
    # go get the first line of text, so we can extract the datetime field
    firstline = read_firstline(filename)[0].split(',')
    plot_datetime = fix_datetime(firstline[0])   # clean up the datetime format
    # read in the battery stats csv file data:
    f = open(filename, 'r') #open file
    try:
        r = csv.reader(f) #init csv reader
        for row in r:
            #assuming we have the format -  datetime, volts, current, watts, air temp, cpu temp, file space %
            xData.append(row[0])
            yData1.append(row[4])
            yData2.append(row[5])
            #print row[0], row[4], row[5]
    finally:
        f.close() #cleanup

    # adjust/cleanup xdata/Datetime data
    xData = [dt.datetime.strptime(d,'%Y-%m-%d_%H%M%S.%f') for d in xData]
    # generate graphs
    fig,v = plt.subplots()
    fig.set_size_inches(12, 6)
    plt.title('Temperature Data: ' + plot_datetime, size=14)
    a = v.twinx()
    v.set
    v.set_xlabel('Time (minutes)', size=14)
    v.set_ylabel('Air Temperature (C)', color='b', size=14)
    a.set_ylabel('CPU Temperature (C)', color='r', size=14)
    for tl in v.get_yticklabels():     # set color to Y axis 1 ticks
        tl.set_color('b')
    for tl in a.get_yticklabels():     # set color to Y axis 2 ticks
        tl.set_color('r')
    formatter = DateFormatter('%d %H:%M')
    plt.gcf().axes[0].xaxis.set_major_formatter(formatter)
    v.plot(xData, yData1, color='b', linestyle='-')
    a.plot(xData, yData2, color='r', linestyle='-')
    fig.savefig('graphs/' + plot_datetime + '_temperature.jpg', bbox_inches='tight', format='jpg')


def generate_diskspace_graph(filename):
    ''' generates a graph from the passed in data file'''
    xData = []
    yData1 = []
    yData2 = []
    # go get the first line of text, so we can extract the datetime field
    firstline = read_firstline(filename)[0].split(',')
    plot_datetime = fix_datetime(firstline[0])   # clean up the datetime format
    # read in the battery stats csv file data:
    f = open(filename, 'r') #open file
    try:
        r = csv.reader(f) #init csv reader
        for row in r:
            #assuming we have the format -  datetime, volts, current, watts, air temp, cpu temp, file space %
            xData.append(row[0])
            yData1.append(row[6])
            #yData2.append(row[2])
            #print row[0], row[6]
    finally:
        f.close() #cleanup

    # adjust/cleanup xdata/Datetime data
    xData = [dt.datetime.strptime(d,'%Y-%m-%d_%H%M%S.%f') for d in xData]
    # generate graphs
    fig,v = plt.subplots()
    fig.set_size_inches(12, 6)
    plt.title('Disk Space: ' + plot_datetime, size=14)
    a = v.twinx()
    v.set
    v.set_xlabel('Time (minutes)', size=14)
    v.set_ylabel('Free Disk Space (%)', color='b', size=14)
    #a.set_ylabel('Battery Current (mA)', color='r', size=14)
    for tl in v.get_yticklabels():     # set color to Y axis 1 ticks
        tl.set_color('b')
    #for tl in a.get_yticklabels():     # set color to Y axis 2 ticks
    #    tl.set_color('r')
    formatter = DateFormatter('%d %H:%M')
    plt.gcf().axes[0].xaxis.set_major_formatter(formatter)
    v.plot(xData, yData1, color='b', linestyle='-')
    #a.plot(xData, yData2, color='r', linestyle='-')
    fig.savefig('graphs/' + plot_datetime + '_diskspace.jpg', bbox_inches='tight', format='jpg')


# Main program:
with open(graph_filenames, 'rb') as fh:   # read lines from master battery data file
    glines = fh.readlines()
fh.close
graph_lines = set(glines)
#print "Graph List: ",graph_lines

with open(batterystat_filenames, 'rb') as fh:  # read in lines from master graph file list
    battery_lines = fh.readlines()
fh.close
#print "Battery List: ",battery_lines

for line in battery_lines[:-1]:  # check all items, but the last one (it's still being written too)
    # check to see if the corrosponding line/file exists in the graph file
    print "battery line: ",line.strip()
    if line not in graph_lines:
        # file hasn't been graphed, so go do it
        generate_battery_graph(line.strip())
        generate_temperature_graph(line.strip())
        generate_diskspace_graph(line.strip())
        print "Generating graph for: ", line.strip(), "\n"
        # save file name into master graph filename data file
        gfh = open(graph_filenames,'a')
        gfh.write(line) 
        gfh.close()
    else:
        print "Battery file has previously been graphed!\n" 

