#!/bin/sh
#
# this will annotate photos with the datetime they where taken, then rename all the files in the chosen directory
#  and copy them into a new folder.
#
# This is meant to run from a FreeBSD server using a NFS mounted images folders.
#
# Paul Hamilton: 20160807

# Local variables
DIR="/mnt/nfs/flower-pi/images"
NDIR="$DIR/tmp"
FOLDER="2016-07-24"

# list out all the files in the directory
cd $DIR
for  FILE  in $FOLDER/$FOLDER_*[JPGjpg]
do
  NFILE=${FOLDER}_$(printf "%05d.jpg" ${count})
  LABEL=` echo $FILE | cut -d'.' -f-1 | cut -d'/' -f2 `
  LABEL1=`echo $LABEL | cut -d'_' -f1 `
  LABEL2=`echo $LABEL | cut -d'_' -f2 `
  LABEL3=`echo $LABEL2 | cut -c1-2 `
  LABEL4=`echo $LABEL2 | cut -c3-4 `
  LABEL5=`echo $LABEL2 | cut -c5-6 `
  LABEL="${LABEL1} ${LABEL3}:${LABEL4}:${LABEL5}  $(printf "%05d" ${count}) "
  echo "Label: $LABEL on file: $FILE"
  #cp "$FILE" "$NDIR/$NFILE"
  convert $FILE -gravity northwest  -fill white  -pointsize 20 -annotate +5+5 " $LABEL " $NDIR/$NFILE
  count=$((count+1))
done
cd tmp
ffmpeg -i ${FOLDER}_%05d.jpg -vcodec mpeg4 -r 20 -b 1800k timelapse_${FOLDER}-7.mp4
