#!/bin/sh
#
# this script just adds a blocking flag file, which then prevents the flower-pi.sh cron started script from running.
# See the corresponding 'start_flowerpi.sh' script that removes the blocking script and allows the flower-pi.py script to run.
#
# Also see the 'kill_flower-pi.sh' script that just kills all current python processes (I hope the temperature python script isn't running!
#
#
#  Paul Hamilton 2016-08-13

/usr/bin/touch /tmp/block_flowerpi
sudo killall python
sudo python /home/pi/flower-pi/ir_flood_OFF.py 
