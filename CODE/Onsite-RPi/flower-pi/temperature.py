#!/usr/bin/python
'''  This Python script will record temperatures into a Temperature log file

  Paul Hamilton: 2016-07-11

 ToDo:


 * log data to a log file
 * if no flag file found, test for DS18B20 and if found, start a new masterfile log filename.  Then creat the flag file to kick of temperature logging

crontab - 1/minute
 check for flag file
   check for DS18B20
     record temperature to data file
   if no DS18B20, write error to screen, don't record error to file

 if no flag file:
   # must be first time it's run on this fresh startup
   create one in /tmp dir. and record filename to master file
 * monitor battery voltage and USB flash drive space.  Halt flower-pi and IR LEDs if flash is full.  Shut down IR and RPi if battery lotage is too low. log both events to a log file 


'''

# set module imports
import os, sys, glob
from datetime import datetime, timedelta
from time import sleep

from Subfact_ina219 import INA219
#import wiringpi
#from oled.device import ssd1306, sh1106
#from oled.render import canvas
#from PIL import ImageFont
#import Image
#import ImageDraw
#import ImageFont
#import picamera


#top = 2              # OLED display top
#x = 0                # OLED display left hand side
#DATETIME = datetime.now().strftime("%Y-%m-%d %H:%M:%S")

flagfile = "/tmp/pollinatorpi.run"               # flag file that controls if the program is running or not

DATADIR = "/home/pi/flower-pi"  # Project Directory
TEMPDATAFILE = DATADIR + "/stats/temperature_stats" + datetime.now().strftime("%Y-%m-%d_%H%M%S.csv")
masterfile = DATADIR + "/datafiles_temperature.txt"      # contains a master list of temperature data file names

#temp_sensor = sys/bus/w1/devices/28-000005e2fdc3/w1_slave
os.system('modprobe w1-gpio')
os.system('modprobe w1-therm')

base_dir = '/sys/bus/w1/devices/'
device_folder = glob.glob(base_dir + '28*')[0]
device_file = device_folder + '/w1_slave'
 

def flag_add(path):
    ''' create the flag file'''
    with open(path, 'a'):
        os.utime(path, None)


def read_temp_raw():
    f = open(device_file, 'r')
    lines = f.readlines()
    f.close()
    return lines
 

def read_temp():
    lines = read_temp_raw()
    while lines[0].strip()[-3:] != 'YES':
        time.sleep(0.2)
        lines = read_temp_raw()
    equals_pos = lines[1].find('t=')
    if equals_pos != -1:
        temp_string = lines[1][equals_pos+2:]
        temp_c = float(temp_string) / 1000.0
        temp_f = temp_c * 9.0 / 5.0 + 32.0
        return temp_c


def test_for_ds18b20():
    ''' this just tests to see if the device is present'''
    if os.path.exists(device_file):
        ''' device must be present'''
        #ds18b20 = 1
        return true
    else:
        #ds18b20 = -1
        return false


def write_display(line1, line2, line3):
    '''Write out three lines of data to the dx.com clone/Adafruit i2c OLED Display'''
    # now show the stats on the OLED Display
    TEXT1 = "V: %.2f V" % line1
    TEXT2 = "A: %d mA" % line2
    TEXT3 = "W: %.1f W" % line3
    with canvas(device) as draw:
        draw.text((x, top),    TEXT1,  font=font, fill=255)
        draw.text((x, top+10), TEXT2, font=font, fill=255)
        draw.text((x, top+20), TEXT3, font=font, fill=255)

def write_text(line1, line2, line3, line4):
    '''Write out three lines of text to the dx.com clone/Adafruit i2c OLED Display'''
    # now show the stats on the OLED Display
    TEXT1 = line1
    TEXT2 = line2
    TEXT3 = line3
    TEXT4 = line4
    with canvas(device) as draw:
        draw.text((x, top),    TEXT1,  font=font, fill=255)
        draw.text((x, top+10), TEXT2, font=font, fill=255)
        draw.text((x, top+20), TEXT3, font=font, fill=255)
        draw.text((x, top+30), TEXT4, font=font, fill=255)


def logdata(current_time, filename, cputemp, airtemp, volts, amps, watts,df_percentage):
    ''' writes out data to logfile'''
    text = current_time + "," + cputemp + "," + airtemp + "," + volts + "," + amps + "," + watts + "," + df_percentage + "\n"

    if os.path.isfile(filename):
        # if data file exists, then write to it, otherwise error?????
        # write stats line to text file:
        fh = open(filename,'a')
        fh.write(text) # python will convert \n to os.linesep
        fh.close()
        print text, "DATA WRITTEN OUT!"
    else:
        print "error with: ", text, filename, "ERROR: NO DATA WRITTEN OUT!"


def write_masterfile(line1):
        current_log = "batterystats_" + log_time + ".txt"
        # write line to text file:
        print "New battery!  Create new logfile..."
        fh = open(masterfile,'a')
        fh.write(line1 + "\n") # python will convert \n to os.linesep
        fh.close()
        # create a new blank file.  This could somehow be incorporated into the current_log file test???
        fh = open(statsdir + "/" + line1,'a')
        fh.close()


def cpu_temp():
    '''Go read the CPU temperature and return a decimal value'''
    temp1 = open('/sys/class/thermal/thermal_zone0/temp', 'r')
    cpu_temp = temp1.read()
    temp1.close()
    return float(cpu_temp) * 0.001


def read_firstline(filename):
     ''' returns the first line of the file'''
     fh = open(filename,"r")
     lines = fh.readlines()
     return lines[0]


def read_lastline(filename):
     ''' returns the last line of the file'''
     fh = open(filename,"r")
     lines = fh.readlines()
     file_length = len(lines)
     return lines[file_length - 1]


def create_masterfile(filename):
     ''' creates a new masterfile entry, and a blank datafile, ready for data entry ''' 
     fh = open(masterfile,'a')
     fh.write(filename + "\n") # python will convert \n to os.linesep
     fh.close()
     # create a new blank data file.  
     fh = open(filename,'a')
     fh.close()


def battery_current():
    '''Go get the current battery power usage'''
    ina = INA219()
    return ina.getCurrent_mA()


def battery_voltage():
    '''Go get the current battery Voltage usage'''
    ina = INA219()
    return ina.getBusVoltage_V()

def filespace(mount):
    statvfs = os.statvfs(mount)
    bytes_available = float(statvfs.f_frsize * statvfs.f_bavail)
    mount_bytes = float(statvfs.f_frsize * statvfs.f_blocks)
    mount_percentage = round(bytes_available / mount_bytes * 100, 1)
    return mount_percentage

# main program
#font = ImageFont.load_default()
#device = sh1106(port=1, address=0x3C)

if os.path.exists(flagfile):
    ''' normal operation - run the normal temperature routine '''
    print "Flagfile: Exists - record data"
    NOW = datetime.strftime(datetime.now(), "%Y-%m-%d_%H%M%S.%f")
    battery_volts = battery_voltage()
    battery_amps = battery_current()
    watts = battery_volts * battery_amps / 1000
    cpu_temperature =  str(cpu_temp())
    print "CPU Temp: ", cpu_temperature
    air_temperature = str(read_temp())
    print "Air Temperature: ", air_temperature
    free_space = filespace("/mnt/usb")
    print NOW, cpu_temperature, air_temperature, battery_volts, battery_amps, watts, free_space
    datafilename = read_lastline(masterfile).rstrip()
    print  datafilename
    # write out data to log file
    logdata( NOW, datafilename, str(battery_volts), str(battery_amps), str(watts), air_temperature, cpu_temperature, str(free_space) )
else:
    ''' path doesn't exist, so create new flagfile and store name in masterfile '''
    print "Flagfile: Does Not Exist! - create file"
    flag_add(flagfile)
    create_masterfile(TEMPDATAFILE)


