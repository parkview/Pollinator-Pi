#!/usr/bin/python
#
# this script is called from a shell script in the shutdown script directory:  /usr/lib/systemd/system-shutdown/
#   It just displays some msg lines onto the OLED screen
#
# Paul Hamilton 2016-08-14

import os, sys, glob, shutil
import subprocess


def make_dir(path):
    if not os.path.isdir(path):
        os.mkdir(path)


def copy_dir(source_item, destination_item):
    if os.path.isdir(source_item):
        make_dir(destination_item)
        sub_items = glob.glob(source_item + '/*')
        for sub_item in sub_items:
            copy_dir(sub_item, destination_item + '/' + sub_item.split('/')[-1])
    else:
        shutil.copy(source_item, destination_item)


def backup_files():
    ''' this will backup the stats and graphs to the USB drive '''
    copy_dir(srcdir1, dstdir1)
    copy_dir(srcdir2, dstdir2)


# main program
srcdir1 = "/home/pi/flower-pi/graphs"
srcdir2 = "/home/pi/flower-pi/stats"
dstdir1 = "/media/pi/usb64/graphs"
dstdir2 = "/media/pi/usb64/stats"

backup_files()

print "backup done!"
