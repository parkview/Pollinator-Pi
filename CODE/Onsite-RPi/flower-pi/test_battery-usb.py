#!/usr/bin/python
#
# this script is called from a shell script in the shutdown script directory:  /usr/lib/systemd/system-shutdown/
#   It just displays some msg lines onto the OLED screen
#
# Paul Hamilton 2016-08-14

import os, sys, glob, shutil
from datetime import datetime, timedelta
from Subfact_ina219 import INA219
import wiringpi
import subprocess
shtdwn_script = "display_shutdown_MSG.py"
volts_low = 11.0
space_low = 0.15


def battery_voltage():
    '''Go get the current battery Voltage usage'''
    ina = INA219()
    return ina.getBusVoltage_V()


def filespace(mount):
    ''' look up and return the amount of drive space left (in GB)'''
    statvfs = os.statvfs(mount)
    bytes_available = float(statvfs.f_frsize * statvfs.f_bavail)
    mount_bytes = float(statvfs.f_frsize * statvfs.f_blocks)
    mount_percentage = round(bytes_available / mount_bytes * 100, 1)
    return mount_percentage


def shutdown():
    command = "/usr/bin/sudo /sbin/shutdown -H now"
    import subprocess
    process = subprocess.Popen(command.split(), stdout=subprocess.PIPE)
    output = process.communicate()[0]
    print output


def make_dir(path):
    if not os.path.isdir(path):
        os.mkdir(path)


def copy_dir(source_item, destination_item):
    if os.path.isdir(source_item):
        make_dir(destination_item)
        sub_items = glob.glob(source_item + '/*')
        for sub_item in sub_items:
            copy_dir(sub_item, destination_item + '/' + sub_item.split('/')[-1])
    else:
        shutil.copy(source_item, destination_item)


def backup_files():
    ''' this will backup the stats and graphs to the USB drive '''
    copy_dir(srcdir1, dstdir1)
    copy_dir(srcdir2, dstdir2)


# main program
battery_volts = str(battery_voltage())
free_space = filespace("/media/pi/usb64")
srcdir1 = "/home/pi/flower-pi/graphs"
srcdir2 = "/home/pi/flower-pi/stats"
dstdir1 = "/media/pi/usb64/graphs"
dstdir2 = "/media/pi/usb64/stats"
#print free_space, "GB"
#print "Battery: ", battery_volts, "V"

if battery_volts < volts_low:
  ''' display shutdown msg and shut down RPi '''
  print "Shutting down RPi - battery voltage is to low"
  print "Battery: ", battery_volts, "V"
  subprocess.call(["python", "shtdwn_script"])
  shutdown()

if free_space < space_low:
  ''' USB drive is running out of free space.  Copy graph files over and shutdown RPi '''
  print "Shutting down RPi - USB drive has run out of free space"
  print free_space, "GB"
  subprocess.call(["python", "shtdwn_script"])
  shutdown()

