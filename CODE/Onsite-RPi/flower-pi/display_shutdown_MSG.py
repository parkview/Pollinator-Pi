#!/usr/bin/python
#
# this script is called from a shell script in the shutdown script directory:  /usr/lib/systemd/system-shutdown/
#   It just displays some msg lines onto the OLED screen
#
# Paul Hamilton 2016-08-14

import os, sys, glob
from datetime import datetime, timedelta
from oled.device import ssd1306, sh1106
from oled.render import canvas
from PIL import ImageFont
from Subfact_ina219 import INA219
import wiringpi


def write_text(line1, line2, line3, line4):
    '''Write out three lines of text to the dx.com clone/Adafruit i2c OLED Display'''
    # now show the stats on the OLED Display
    TEXT1 = line1
    TEXT2 = line2
    TEXT3 = line3
    TEXT4 = line4
    with canvas(device) as draw:
        draw.text((x, top),    TEXT1,  font=font, fill=255)  # current datetime
        draw.text((x, top+10), TEXT2, font=font, fill=255)   # current battery voltage
        draw.text((x, top+20), TEXT3, font=font, fill=255)   # USB Drive - free space
        draw.text((x, top+30), TEXT4, font=font, fill=255)   # RPi Uptime


def battery_voltage():
    '''Go get the current battery Voltage usage'''
    ina = INA219()
    return ina.getBusVoltage_V()


def led_off():
     '''force the LED flood light off'''
     wiringpi.digitalWrite(LED,0)    # set LED flood light pin to off


def filespace(mount):
    ''' look up and return the amount of drive space left (in GB)'''
    statvfs = os.statvfs(mount)
    bytes_available = float(statvfs.f_frsize * statvfs.f_bavail)
    mount_bytes = float(statvfs.f_frsize * statvfs.f_blocks)
    mount_percentage = round(bytes_available / mount_bytes * 100, 1)
    return mount_percentage


def uptime():
    ''' returns a formatted string with the RPi UPTIME '''
    with open('/proc/uptime', 'r') as f:
        uptime_seconds = float(f.readline().split()[0] )
        uptime_string =  str(timedelta(seconds = uptime_seconds)).split(".", 1)[0]
        return uptime_string


# main program
LED = 11             # IR LED is on pin 11
top = 2              # OLED display top
x = 0                # OLED display left hand side
wiringpi.wiringPiSetupGpio()   # set GPIO mode to GPIO pin numbers
wiringpi.pinMode(LED,1)        # set pin 15 (led) as output
font = ImageFont.load_default()
device = sh1106(port=1, address=0x3C)
battery_volts = str(battery_voltage())
led_off()
free_space = filespace("/media/pi/usb64")
NOW = datetime.strftime(datetime.now(), "%a %H:%M:%S")

write_text("END: " + NOW, "Batt: " + battery_volts + "V","USB: " + str(free_space) + "GB free", "UP: " + uptime() )

