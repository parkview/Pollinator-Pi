#!/bin/sh
#
# this script will start the python script running
#
#

COUNT=`ps ax | grep flower-pi.py | grep -v "grep" | wc -l`
echo $COUNT
if [ $COUNT -lt 2 ] && [ ! -f /tmp/block_flowerpi ]
then
  sudo /usr/bin/python /home/pi/flower-pi/flower-pi.py
  echo "Starting Flower-Pi"
else
  echo "Flower-Pi already running"
fi
