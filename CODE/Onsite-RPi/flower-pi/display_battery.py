#!/usr/bin/python

from Subfact_ina219 import INA219
from oled.device import ssd1306, sh1106
from oled.render import canvas
from PIL import ImageFont

ina = INA219()
result = ina.getBusVoltage_V()

ir_status = "OFF"
airtemp = 99.9
volts = ina.getBusVoltage_V()
print "Volts:    : %.3f V" % volts
current = ina.getCurrent_mA()
print "Current : %.3f mA" % current
watts = volts * current /1000
print "Power: %.2f W" % watts


font = ImageFont.load_default()
device = sh1106(port=1, address=0x3C)

with canvas(device) as draw:
    # Draw some shapes.
    # First define some constants to allow easy resizing of shapes.
    padding = 2
    shape_width = 20
    top = padding
    bottom = device.height - padding - 1
    # Draw a rectangle of the same size of screen
    #draw.rectangle((0, 0, device.width-1, device.height-1), outline=255, fill=0)
    # Move left to right keeping track of the current x position for drawing shapes.
    x = padding
    # Draw an ellipse.
    #draw.ellipse((x, top, x+shape_width, bottom), outline=255, fill=0)
    # Draw a rectangle.
    #draw.rectangle((x, top, x+shape_width, bottom), outline=255, fill=0)
    # Draw a triangle.
    #draw.polygon([(x, bottom), (x+shape_width/2, top), (x+shape_width, bottom)], outline=255, fill=0)

    # Load default font.
    font = ImageFont.load_default()

    # Write two lines of text.
    draw.text((x, top),    'Air Temp:' + str(airtemp),  font=font, fill=255)
    draw.text((x, top+11), 'Volts:' + str(volts) + "V", font=font, fill=255)
    #draw.text((x, top+18), 'Amps:' + str(current) + "mA", font=font, fill=255)
    draw.text((x, top+22), 'Watts:' + str(watts) + "W", font=font, fill=255)
    draw.text((x, top+33), 'IR Light:' + ir_status, font=font, fill=255)
