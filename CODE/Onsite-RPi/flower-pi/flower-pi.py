#!/usr/bin/python
'''  This Python script is set to take photos of flower - looking for and tracking insects or the pollinators of flowers

  Paul Hamilton: 2016-06-26

# ToDo:
#

 * could create a pygame based dash board:
     * image count
     * image display
     * image zoom
     * voltage/current
     * insect ount
     * datetime

 * put together a USB hub kit, PSU, 12V adaptor
 * auto shut script and RPi down if the USB drive or battery voltage drops too low.  
 * Write a msg to screen on shutdown: battery voltage, USB HD space, time, cpu/air temps.
 * create a watchdog cron script that will restart flower-pi.sh if it's not running.  Use a flag file, but have kill_python script stop this one from running
 * DONE: 2016-08-12 - fix hw clock sync to RPi properly
 * DONE: 2016-07-28 - test out inserting new/different USB stick - do they keep the same names?
 * DONE: 2016-07-22 - fix USB mounting issue
 * Done: 2016-07-16 - find green camaflage sheet.  Test out with my poncho, or ground mat from Diane.
 * Done: 2016-07-16 - waterproof LED IR light
 * Done: 2016-07-14 - get the IR flood light controlled via program.  Pin 11 (LED's don't quite turn off 100%)
 * Done: 2016-07-13 - log data to a log file
 * Done: 2016-07-13 - copy over battery-pi files and setup crontab job - 1/min
 * Done: 2016-07-13 - copy over temp data files and setup crontab job - 1/min
 * Done: 2016-07-14 - get the 9" HDMIPi Screen going
 * Done: 2016-07-14 - turn off the camera LED.  Added permanent line to /boot/config.txt
 * DONE: 2016-07-11 - waterproof Temp. 
 * DONE: 2016-07-11 - seal up/waterproof lid.
 * DONE: 2016-07-11 - waterproof camera

'''

# set module imports
import os, sys, glob
from Subfact_ina219 import INA219
from datetime import datetime, timedelta
import wiringpi
from oled.device import ssd1306, sh1106
from oled.render import canvas
from PIL import ImageFont
from time import sleep
import Image
import ImageDraw
import ImageFont
import picamera


DATADIR = "/home/pi/flower-pi"
DATAFILE = DATADIR + "/battery_stats" + datetime.now().strftime("%Y-%m-%d_%H%M%S.csv")
DATETIME = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
top = 2              # OLED display top
x = 0                # OLED display left hand side
volts_low = 10.85    # SLA battery test cut off.  Turn off the RPi when we get to this voltage
dirimages = "/mnt/usb"
image_filename = dirimages
LED = 11
DATADIR = "/home/pi/battery/ina219"
DATAFILE = DATADIR + "/battery_stats" + datetime.now().strftime("%Y-%m-%d_%H%M%S.csv")
localdir = "/home/pi/battery"                 # project directory
masterfile = localdir + "/datafiles.txt"      # contains a list of data file names

#temp_sensor = sys/bus/w1/devices/28-000005e2fdc3/w1_slave
os.system('modprobe w1-gpio')
os.system('modprobe w1-therm')

base_dir = '/sys/bus/w1/devices/'
device_folder = glob.glob(base_dir + '28*')[0]
device_file = device_folder + '/w1_slave'
 
def read_temp_raw():
    f = open(device_file, 'r')
    lines = f.readlines()
    f.close()
    return lines
 

def read_temp():
    lines = read_temp_raw()
    while lines[0].strip()[-3:] != 'YES':
        time.sleep(0.2)
        lines = read_temp_raw()
    equals_pos = lines[1].find('t=')
    if equals_pos != -1:
        temp_string = lines[1][equals_pos+2:]
        temp_c = float(temp_string) / 1000.0
        temp_f = temp_c * 9.0 / 5.0 + 32.0
        return temp_c


def write_display(line1, line2, line3):
    '''Write out three lines of data to the dx.com clone/Adafruit i2c OLED Display'''
    # now show the stats on the OLED Display
    TEXT1 = "V: %.2f V" % line1
    TEXT2 = "A: %d mA" % line2
    TEXT3 = "W: %.1f W" % line3
    with canvas(device) as draw:
        draw.text((x, top),    TEXT1,  font=font, fill=255)
        draw.text((x, top+10), TEXT2, font=font, fill=255)
        draw.text((x, top+20), TEXT3, font=font, fill=255)

def write_text(line1, line2, line3, line4):
    '''Write out three lines of text to the dx.com clone/Adafruit i2c OLED Display'''
    # now show the stats on the OLED Display
    TEXT1 = line1
    TEXT2 = line2
    TEXT3 = line3
    TEXT4 = line4
    with canvas(device) as draw:
        draw.text((x, top),    TEXT1,  font=font, fill=255)
        draw.text((x, top+10), TEXT2, font=font, fill=255)
        draw.text((x, top+20), TEXT3, font=font, fill=255)
        draw.text((x, top+30), TEXT4, font=font, fill=255)


def logdata(current_time, filename, photoname):
#def logdata(current_time, filename, cputemp, airtemp, volts, amps, watts):
    ''' writes out data to logfile'''
    text = current_time + "," + photoname + "\n"
    #text = current_time + "," + cputemp + "," + airtemp + "," + volts + "," + amps + "," + watts + "\n"

    if os.path.isfile(filename):
        # if data file exists, then write to it, otherwise error?????
        # write stats line to text file:
        fh = open(filename,'a')
        fh.write(text) # python will convert \n to os.linesep
        fh.close()
        print text, "DATA WRITTEN OUT!"
    else:
        print "error with: ", text, filename, "ERROR: NO DATA WRITTEN OUT!"


def write_masterfile(line1):
        current_log = "batterystats_" + log_time + ".txt"
        # write line to text file:
        print "New battery!  Create new logfile..."
        fh = open(masterfile,'a')
        fh.write(line1 + "\n") # python will convert \n to os.linesep
        fh.close()
        # create a new blank file.  This could somehow be incorporated into the current_log file test???
        fh = open(statsdir + "/" + line1,'a')
        fh.close()

def turnoffload():
    '''turns off the battery load switch MOSFET'''
    wiringpi.digitalWrite(LOAD,0)    # set pin 10 (Battery MOSFet off) to low
    led_off()                         # turn off RED status LED
    os.remove(flagfile)               # remove flag file in the temp dir.  this will stop it from running again


def battery_current():
    '''Go get the current battery power usage'''
    ina = INA219()
    return ina.getCurrent_mA()


def battery_voltage():
    '''Go get the current battery Voltage usage'''
    ina = INA219()
    return ina.getBusVoltage_V()


def cpu_temp():
    '''Go read the CPU temperature and return a decimal value'''
    temp1 = open('/sys/class/thermal/thermal_zone0/temp', 'r')
    cpu_temp = temp1.read()
    temp1.close()
    return float(cpu_temp) * 0.001



def led_on():
     '''force the LED flood light on'''
     wiringpi.digitalWrite(LED,1)    # set LED flood light pin to on


def led_off():
     '''force the LED flood light off'''
     wiringpi.digitalWrite(LED,0)    # set LED flood light pin to off


def take_photo(filename):
    ''' takes a single photo with specified filename '''
    camera.start_preview()
    # Camera warm-up time
    sleep(.2)
    #sleep(1)
    camera.capture(filename)


def filespace(mount):
    ''' look up and return the amount of dirve space left '''
    statvfs = os.statvfs(mount)
    bytes_available = float(statvfs.f_frsize * statvfs.f_bavail)
    mount_bytes = float(statvfs.f_frsize * statvfs.f_blocks)
    mount_percentage = round(bytes_available / mount_bytes * 100, 1)
    return mount_percentage


# main program
font = ImageFont.load_default()
device = sh1106(port=1, address=0x3C)
#count = 15
count = 36800   # 16381 photos in approx 5hr 33min. = ~ 21GB of used space at 1920x1080 resolution!
#count = 3600   # 2600 photos in approx 43min. = ~ 7.2GB of used space at full resolution!

wiringpi.wiringPiSetupGpio()   # set GPIO mode to GPIO pin numbers
wiringpi.pinMode(LED,1)        # set pin 15 (led) as output

#cpu_temperature =  str(cpu_temp())
#print "CPU Temp: ", cpu_temperature
bvolts = battery_voltage()
bamps = battery_current()
watts = bvolts * bamps / 1000
print "LED OFF Watts: ", watts
#led_on()
sleep(1)
bvolts = battery_voltage()
bamps = battery_current()
watts = bvolts * bamps / 1000
#print "LED ON Watts: ", watts
NOW = datetime.strftime(datetime.now(), "%Y-%m-%d_%H%M%S.%f")
write_text("V: "+str(battery_voltage()), "mA: "+str(battery_current()), "W: "+str(watts), "DT: "+ NOW)
#write_display(battery_voltage(), battery_current(), watts, NOW)

# setup camera config
camera = picamera.PiCamera()
camera.led = False
camera.resolution = (1920, 1080)
#camera.resolution = (2592, 1944)
#camera.resolution = (1024, 768)
#camera.brightness = 60

# sleep for the next 90 seconds to allow user to halt program
sleep(90)

#air_temperature = str(read_temp())
#print "Air Temperature: ", air_temperature
for loop in range(0, count):
    NOW = datetime.strftime(datetime.now(), "%Y-%m-%d_%H%M%S-%f")
    filename=image_filename+"/"+NOW+".JPG"
    print filename
    led_on()
    take_photo(filename)
    led_off()
    battery_volts = str(battery_voltage())
    #battery_amps = str(battery_current())
    #air_temperature = str(read_temp())
    cpu_temperature =  str(cpu_temp())
    #print "CPU Temp: ", cpu_temperature
    # update OLED display
    write_text("Count: "+str(loop), "V: "+battery_volts, "CPU: "+cpu_temperature, " ")
    #write_text("Count: "+str(loop), "V: "+battery_volts, "CPU: "+cpu_temperature, "AirT: "+air_temperature)
    # write out data to log file
    #logdata( NOW, photodatafile, filename )
    #logdata( NOW, photodatafile, filename, battery_volts, battery_amps, str(watts), air_temperature, cpu_temperature )
    #sleep(1)

print "LED Off"
led_off()

NOW = datetime.strftime(datetime.now(), "%Y-%m-%d_%H%M%S.%f")
write_text("Count: "+str(loop)/str(count), "V: "+battery_volts, "CPU: "+cpu_temperature, "END: " + NOW)
exit()
