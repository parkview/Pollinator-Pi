
# set module imports
import os, sys
from datetime import datetime, timedelta
import wiringpi


LED = 11	     # IR flood light 


def led_on():
     '''force the status LED on'''
     wiringpi.digitalWrite(LED,1)    # set status LED pin to on


def led_off():
     '''force the status LED off'''
     wiringpi.digitalWrite(LED,0)    # set status LED pin to off



# main program

wiringpi.wiringPiSetupGpio()   # set GPIO mode to GPIO pin numbers
wiringpi.pinMode(LED,1)        # set pin 11 (led) as output

led_on()
