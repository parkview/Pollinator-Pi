#!/bin/sh
#
# this will kill all Python processes that are currently running
#
# Paul Hamilton 2016-08-05

ps ax | grep python
sudo killall python

