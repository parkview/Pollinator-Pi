#!/bin/sh
#
#  This will backup important files that are used for this Raspberry Pi Project
#
#  Paul Hamilton 2014-01-27
#

# Local variables
BACKUPDIR="/home/pi/os-files/"


# Main
cp /etc/network/interfaces  $BACKUPDIR
cp /etc/network/interfaces.NIC $BACKUPDIR
cp /etc/network/interfaces.wireless.AP $BACKUPDIR
cp /etc/udhcpd.conf $BACKUPDIR
cp /etc/hostapd/hostapd.conf $BACKUPDIR
cp /etc/hostapd/hostapd.conf $BACKUPDIR
cp /etc/samba/smb.conf $BACKUPDIR
cp /etc/ntp.conf $BACKUPDIR
cp /etc/modprobe.d/8192cu.conf $BACKUPDIR
cp /boot/config.txt $BACKUPDIR
cp /etc/inittab $BACKUPDIR
cp /etc/modules $BACKUPDIR
cp /etc/modprobe.d/raspi-blacklist.conf $BACKUPDIR
cp /etc/rc.local $BACKUPDIR
cp /etc/rc.shutdown $BACKUPDIR
cp /etc/fstab $BACKUPDIR
cp /home/pi/temperature/accesspoint-start.sh $BACKUPDIR
cp /home/pi/temperature/accesspoint-stop.sh $BACKUPDIR
cp /home/pi/.bashrc $BACKUPDIR
cp /home/pi/pushbullet.sh $BACKUPDIR
cp /home/pi/backup_os_files.sh $BACKUPDIR
sudo cp /var/spool/cron/crontabs/pi $BACKUPDIR
sudo chmod 744 $BACKUPDIR/pi
#cp  $BACKUPDIR
#cp xyz $BACKUPDIR

