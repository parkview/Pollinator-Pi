#!/bin/bash
#
#  sends out a pushbullet notification to an account

cd /home/pi/
sleep 10  # wait for network setup to conclude

API="Add-API-KEY"
DATETIME=`date "+ %Y-%m-%d %H:%M:%S: "`
HOSTNAME=`hostname | tr '[:lower:]' '[:upper:]' `
IPADD=$(/sbin/ifconfig eth0 | grep 'inet addr:' | cut -d: -f2 | awk '{ print $1}')
MSG=" '$HOSTNAME' $IPADD has booted up at: $DATETIME!"

curl -u $API: https://api.pushbullet.com/v2/pushes -d type=note -d title="RPi Booted up:" -d body="$MSG"  >/tmp/BOOT.out 2>/tmp/BOOT.err


