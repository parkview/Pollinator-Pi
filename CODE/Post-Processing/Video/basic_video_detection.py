# USAGE
# python motion_detector.py
# python motion_detector.py --video videos/example_01.mp4  -a 50 --max-area 1000 -p 30 -s 800
#
#  NOTE:  Python 2.7 environment

#  ToDo's:
#-----------
#
# change script over to Python 3.x
# * change fixed log file name to include the date of the video file
# * dump motion video to file,
# * could draw a timeline of events at the bottom of the screen.  This would display all movements - much like Guetenbruk
# * create a PyGame front end that extracts frame 1000 and allows me to draw the ignore/flower rectangles and then feeds 
#       them into the OpenCV script.
# * how about creating a special OpenCV script that creates a heatmap of all tracked objects - this might show insect 
#       paths or 'hot' items
# * add in a special zone mask argument.  This would just draw the zones and pause untill user presses Q

# * DONE:  2016-08-27 - try a larger video size - 800px works much better and just a bit slower
# * Done: 2016-08-27 - log fame movement stats to a csv or text log file
# * Done: 2016-08-28 - allow the flower zone to be drawn.  Only targets in this zone (circle?), would be coloured green, 
#       outside they could be red.  Only these items might be extracted and stored as a full frame jpeg
# * Done: 2016-08-28 - allow ignore zones to be drawn.  This would allow leaf/twig movements to be ignored
# * Done: 2016-08-28 - Dump a large frame photo to file - only if the moving object  is in the flower area of interest. 
#        Will need to autoscale up the small 800px flower zone to match the HD photo
#


# import the necessary packages
from shutil import copyfile    # used to copy file to a new directory
import argparse                # used to parse CLI arguments
import datetime                # play with dates - is this used?
import imutils                 # Adrians OpenCV utility module
import time                    # sleep - only used for live RPi camera use
import cv2                     # OpenCV


# Local Variables
videodate = "2017-08-05"
logfile = "/mnt/nfs/flower-pi/images/video_logfile.tmp" + videodate + ".dat"
motiondir = "/mnt/nfs/flower-pi/images/tmp" + videodate
ignoremask = [790, 41, 799, 50]        # 2016-09-04 video
flowermask = [285, 110, 475, 330]     # 2016-09-04 video -skip 50
# ignore examples:
#ignoremask = [790, 41, 799, 50]        # 2016-09-04 video
#flowermask = [240, 165, 450, 360]     # 2016-09-04 video -skip 50
#ignoremask = [750, 1, 799, 50]        # 2016-09-03 video
#flowermask = [310, 130, 510, 340]     # 2016-09-02 video 2nd flower - skip 0
#ignoremask = [750, 1, 799, 50]        # 2016-09-02 video
#flowermask = [310, 130, 510, 340]     # 2016-09-02 video 2nd flower - skip 310
#ignoremask = [750, 1, 799, 50]        # 2016-09-01 video
#flowermask = [300, 153, 490, 380]     # 2016-09-01 video 2nd flower
#ignoremask = [750, 1, 799, 50]        # 2016-08-31 video
#flowermask = [300, 153, 490, 380]     # 2016-08-31 video 2nd flower
#ignoremask = [10, 13, 11, 14]         # 2016-08-25 video
#flowermask = [300, 240, 490, 400]     # 2016-08-25 video 1st flower
#ignoremask = [10, 13, 11, 14]         # 2016-08-24 video
#flowermask = [290, 230, 495, 410]     # 2016-08-24 video - skip 200
#ignoremask = [10, 13, 11, 14]         # 2016-08-21 video
#flowermask = [390, 110, 550, 265]     # 2016-08-21 video - skip 0
#ignoremask = [10, 13, 11, 14]         # 2016-08-20 video
#flowermask = [390, 110, 550, 265]     # 2016-08-20 video - skip 0
#ignoremask = [10, 13, 11, 14]         # 2016-08-19 video
#flowermask = [390, 110, 550, 265]     # 2016-08-19 video - skip 0
#ignoremask = [10, 13, 11, 14]         # 2016-08-14 video
#flowermask = [330, 160, 520, 375]     # 2016-08-14 video - skip 0
#ignoremask = [10, 13, 11, 14]         # 2016-08-12 video
#flowermask = [330, 125, 540, 350]     # 2016-08-12 video - skip 240
#ignoremask = [10, 130, 90, 310]       # 2016-07-31 video
#flowermask = [390, 110, 570, 300]     # 2016-07-31 video - skip 0
#ignoremask = [10, 130, 135, 300]      # 2016-07-30 video
#flowermask = [390, 110, 570, 300]     # 2016-07-30 video - skip 58
#ignoremask = [10, 13, 11, 14]         # 2016-07-24 video
#flowermask = [110, 230, 440, 449]     # 2016-07-24 video - skip 40
false = 0
true = 1


def logdata(line):
    ''' this writes out a block of log lines to a text file'''
    fh = open(logfile,'a')
    fh.write(line + "\n") # python will convert \n to os.linesep
    fh.close()


def checkignore(X1, Y1, W1, H1):
    ''' check to see if the passed bounding box of the object sits within an ignored mask area '''
    X2 = ignoremask[0]
    Y2 = ignoremask[1]
    W2 = ignoremask[2] - X2
    H2 = ignoremask[3] - Y2
    if (X1+W1<X2 or X2+W2<X1 or Y1+H1<Y2 or Y2+H2<Y1):
        return false
    else:
        return true


def checkflower(X1, Y1, W1, H1):
    ''' check to see if the passed bounding box of the object sits within the flower mask area '''
    X2 = flowermask[0]
    Y2 = flowermask[1]
    W2 = flowermask[2] - X2
    H2 = flowermask[3] - Y2
    if (X1+W1<X2 or X2+W2<X1 or Y1+H1<Y2 or Y2+H2<Y1):
        return false
    else:
        return true


# construct the argument parser and parse the arguments
ap = argparse.ArgumentParser()
ap.add_argument("-v", "--video", help="path to the video file")
ap.add_argument("-a", "--min-area", type=int, default=40, help="minimum area size")
ap.add_argument("-m", "--max-area", type=int, default=1000, help="maximum area size")
ap.add_argument("-b", "--blank", type=int, default=50, help="reference frame blank rate")
ap.add_argument("-s", "--size", type=int, default=800, help="resultant frame size")
ap.add_argument("-p", "--pass", type=int, default=0, help="skip past xx number of initial frames")
ap.add_argument("-z", "--zone", type=int, default=0, help="show Zones and then pause")
args = vars(ap.parse_args())

# if the video argument is None, then we are reading from webcam
if args.get("video", None) is None:
	camera = cv2.VideoCapture(0)
	time.sleep(0.25)

# otherwise, we are reading from a video file
else:
	camera = cv2.VideoCapture(args["video"])

# initialize the first frame in the video stream
firstFrame = None

ref_frame_count = args["blank"]      # the count of when a new reference frame is taken
# print out min/max setings:
print "Min_Area: ", args["min_area"], " Max_Area: ", args["max_area"], " Blank: ", ref_frame_count, " Size: ", args["size"], " Pass: ", args["pass"], " Zone: ", args["zone"]
# loop over the frames of the video
framecount = 0   # number of the current frame being prcessed
skip = 0         # whether or not we skip this frame, 0 = don't skip, 1 = skip the frame
frameflag = 0
while True:
	# grab the current frame and initialize the occupied/unoccupied
	# text
	(grabbed, frame1) = camera.read()
	text = " "

	# if the frame could not be grabbed, then we have reached the end
	# of the video
	if not grabbed:
		break

	# resize the frame, convert it to grayscale, and blur it
	frame = imutils.resize(frame1, width=args["size"])
	gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
	gray = cv2.GaussianBlur(gray, (21, 21), 0)

	# if the first frame is None, initialize it
	if firstFrame is None:
		firstFrame = gray
		continue
        # reset reference frame every 50 frames
        #print Mod: ", int(camera.get(cv2.CAP_PROP_POS_FRAMES)), " ", int(camera.get(cv2.CAP_PROP_POS_FRAMES)) % ref_frame_count
        if ( int(camera.get(cv2.CAP_PROP_POS_FRAMES)) % ref_frame_count == 0 ) \
            or ( int(camera.get(cv2.CAP_PROP_POS_FRAMES))  > args["pass"] and skip == 1 ):
                firstFrame = gray
                skip = 0
                continue

	# compute the absolute difference between the current frame and
	# first frame
	frameDelta = cv2.absdiff(firstFrame, gray)
	thresh = cv2.threshold(frameDelta, 25, 255, cv2.THRESH_BINARY)[1]

	# dilate the thresholded image to fill in holes, then find contours
	# on thresholded image
	thresh = cv2.dilate(thresh, None, iterations=2)
        (_, cnts, _) = cv2.findContours(thresh.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)

	# loop over the contours
        lcount = 0
        if  args["zone"] == 0:  # if in zone debug mode, skip the next block and just show the zones
	    for c in cnts:
		# if the area is too small, ignore it
		if ( cv2.contourArea(c) < args["min_area"] ) \
                    or (cv2.contourArea(c) > args["max_area"] ) \
                    or ( int(camera.get(cv2.CAP_PROP_POS_FRAMES))  < args["pass"] + 1 ):
                        #print len(cnts), cv2.contourArea(c), int(camera.get(cv2.CAP_PROP_POS_FRAMES))  > args["pass"]
                        skip = 1
			continue

		# compute the bounding box for the contour, draw it on the frame,
		# and update the text
		(x, y, w, h) = cv2.boundingRect(c)
                # check for masked areas, ie: ignore cv2.c coord. if it's with a masked ignore bounding box
                if checkignore(x, y, w, h):
                    print "ignore object"
                    continue
                if checkflower(x, y, w, h):
                    if frameflag != int(camera.get(cv2.CAP_PROP_POS_FRAMES)):
                        # the object is inside the flower region
		        cv2.rectangle(frame, (x, y), (x + w, y + h), (0, 255, 0), 2)
                        lcount = lcount + 1
                        location = "*"
                        # now go and save the relevant frame to file in the motion directory
                        # needs updating to copy the original frame instead of the video frame
                        path = "{base_path}{filename}.jpg".format(base_path=motiondir + "/motion/", filename="motion" 
                            + str(framecount).zfill(5))
                        path1 = "{base_path}{filename}.jpg".format(base_path=motiondir + "/motion/", filename="motion1" 
                            + str(framecount).zfill(5))
                        #print "[UP] {}".format(path)
                        # copy the current frame file from the good photo directory to the tmp motion directory
                        pathsrc = motiondir + "/" + videodate +  "_" + str(int(camera.get(cv2.CAP_PROP_POS_FRAMES))).zfill(5) + ".jpg"
                        print pathsrc, path, int(camera.get(cv2.CAP_PROP_POS_FRAMES)), frameflag, 
                        copyfile(pathsrc, path)      # copy the annotated good photo into the motion directory
                        #cv2.imwrite(path, frame1)    # copies HD video frame to the motion directory
                        cv2.imwrite(path1, frame)    # copies the reduced size video frame - with markup to motion directory
                        framecount = framecount + 1  # count of the frames we have written out to file so far
                        frameflag = int(camera.get(cv2.CAP_PROP_POS_FRAMES))  # flag to notify that we have already written out the current frame
                else:
                    # must be outside the flower region
		    cv2.rectangle(frame, (x, y), (x + w, y + h), (0, 185, 0), 1)
                    location = ""
                logline = "Obj: " + str(lcount) + ", Frame: " + str(int(camera.get(cv2.CAP_PROP_POS_FRAMES))) + ", Area: " \
                    + str(cv2.contourArea(c)) + ", " + location
                logdata(logline)
                print logline
        else:
            skip = 1

        cv2.rectangle(frame, (ignoremask[0], ignoremask[1]), (ignoremask[2], ignoremask[3]), (255, 255, 0), 2)   # draw ignore mask area
        cv2.rectangle(frame, (flowermask[0], flowermask[1]), (flowermask[2], flowermask[3]), (255, 0, 255), 2)   # draw flower mask area
        if skip == 0:
	    cv2.putText(frame, str(int(camera.get(cv2.CAP_PROP_POS_FRAMES))) + " " + str(len(cnts)),
		(10, frame.shape[0] - 10), cv2.FONT_HERSHEY_SIMPLEX, 0.45, (0, 0, 255), 1)
        else:
	    cv2.putText(frame, str(int(camera.get(cv2.CAP_PROP_POS_FRAMES))) + " SKIP",
		(10, frame.shape[0] - 10), cv2.FONT_HERSHEY_SIMPLEX, 0.45, (0, 0, 255), 1)

	# show the frame and record if the user presses a key
	cv2.imshow("Thresh", thresh)
	cv2.imshow("Frame Delta", frameDelta)
	cv2.imshow("Flower Video", frame)
	key = cv2.waitKey(1) & 0xFF
        frameflag = 0  # reset flag  we can start fresh for the next frame
	# if the `q` key is pressed, break from the lop
	if key == ord("q"):
		break

# cleanup the camera and close any open windows
camera.release()
cv2.destroyAllWindows()

