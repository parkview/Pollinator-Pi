
QUICK HOW TO:-
--------------

from RPi run rsync photos from USB drive to named folder on QNAP
clean up photo files
from FreeBSD server run annotate script, which will annotate and compile video file
from RPi run cmd to process video file via OpenCV to look for movement
watch video to make notes on anything interesting


LONG HOW TO:-
-------------
a) Mount the QNAP NFS share onto RPi:  sudo mount -o nolock 10.137.10.220:/raspberrypi /mnt/nfs

b) Mount USB drive - if it doesn't auto-mount:  sudo mount /dev/sda1 /media/pi/usb64

1) The command below will copy photo data from the USB drive into a new folder called the <date>.  No need to pre-create the folder.  Rsync will auto do this.

 time rsync -a /media/pi/usb64/ /mnt/nfs/flower-pi/images/2017-07-30/   

This is the Master photo folder

2) Create a new temp folder:  

mkdir /mnt/nfs/flower-pi/images/tmp2017-07-31/

record in spreadsheet # of files in folder, and folder size:  l /mnt/nfs/flower-pi/images/2017-07-30/|wc  &  du -h l /mnt/nfs/flower-pi/images/2017-07-30/
From a RPi you can check the folder size by running:  du -hx -d1 /mnt/nfs/flower-pi/images/2017-07-30/

format USB drive or delete files ready to next field use

open NAS folder on laptop and delete all 0 byte files - or write a script to do this auotmaticly.  Need to change folder permissions on NAS via File Station - or write script
record first vaild photo time and last valid photo time in spreadsheet.  Write script to show this info

create a tmp folder - if it doesn't exist:  mkdir /mnt/nfs/flower-pi/images/tmp

 NOTE: rename.sh file, isn't used.  This is an old file

On NANT as root user, mount QNAP NFS:  mount 10.137.10.220:/raspberrypi /mnt/nfs

from the images folder:  /mnt/nfs/flower-pi/images/  edit the FOLDER directory name with current/new date folder name  in the script annotate-images.sh   run the script and record the time taken to run:  time ./annotate-images.sh

record time taken to annotate and rename files into spreadsheet.  This script might also run the video script, or run manually:

cd tmp
ffmpeg -i ${FOLDER}_%05d.jpg -vcodec mpeg4 -r 25 -b 1800k timelapse_${FOLDER}.1.mp4

via laptop Windows Explorer, with RPi share open, copy video file in NFS QNAP tmp folder to //Pollinator-Pi/home/pi/video/ folder.

then need to move tmp folder to a new location:  mv tmp tmp<data>

create a motion sub directory:   mkdir tmp<date>/motion

then re-create a new tmp folder:   mkdir tmp

Now, need to reset permissions:

chmod 777 tmp
chmod 777 tmp<date>
chmod 777 tmp<date>/motion

on the RPi, change to this folder:  /home/pi/video/


startup VNC:  vncserver   ; it will first ask for a password

then on laptop, open a VNC connection to RPi:  10.137.10.150:1

open a terminal window

./setCVenviron.sh   # this will set the Xwin/VNC environment variables correctly

run:   workon cv    # to switch to CV mode.  You will see cv at each prompt for here on

edit script: /home/pi/video/basic_video_detection.py   and enter in video date and update mask areas

run the openCV script to detect insect motion around the flower.  
time python basic_video_detection.py -v timelapse_2016-08-24.1.mp4 -a 40 -m 1000 -b 50 -s 800 -p 10 -z 1 && date

where the switchs mean:
-a  ; min. pixel trigger size
-v  ; video file name
-m  ; max. pixel trigger area
-b  ; reference frame blank rate - how many frames before a new reference frame is used
-s  ; resultant video frame size.  The larger the size, the large the video and slower to create
-p  ; skip past nn number of initial frames (sometimes there is too much initial movement
-z  ; show the zones and then pause.  Used to test zone placement

a data file with insects movements can be found here:  /mnt/nfs/flower-pi/images/   lines with a * mean that an insect was found in the flower zone.

watch video and note down insect movements.

all these scripts are in subversion! - must include this README file as well!


IMPORTANT FILES:-
-----------------

\\pollinator-pi\home\pi\flower-pi\      ; lots of scripts
\\rpi-home\home\pi\video\README.txt   ; this file
\\rpi-home\home\pi\video\annotate-images.sh
\\rpi-home\home\pi\video\basic_video_detection.py
\\rpi-home\home\pi\video\*.mp4   ; list of all video files
/mnt/nfs/flower-pi/images/   ; accessible from RPi (10.137.10.150) or NANT (10.137.10.18). Contains all the raw photos, video and motion photos
/mnt/nfs/flower-pi/images/tmp    ; temporary folder where video is compile and the initial storage of the annotated photos
/mnt/nfs/flower-pi/images/<date>  ; master folder of field photos
/mnt/nfs/flower-pi/images/tmp<date>   ; the above tmp folder is then renamed to this name once processing is finished
\Documents\Electronics\RasberryPi\pollinator-pi  ; laptop - master project files and code backup, Excel data spreadsheets
Documents\My Projects\Flower-Pi   ; some more project files - mainly circuit design
/project/pollinator-pi   ; info on building RPi code and running it all



ON SITE:-
---------

* Before replacing the USB drive.  Find and delete the first photo on the USB drive, then copy over the last few data files and graphs.
* automate the home data/copy/processes some more

