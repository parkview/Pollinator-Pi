Project Name:  Pollinator Pi


I gave a couple of talks in November 2020 on the Pollinator Pi project so that got me excited on creating a PCB to replace my old vero board + RTC + Power distribution vero board.  Note: this PCB could also form the basis for the Photobooth Project as well.

RPi GPIO's:
-----------
Name   -   Pin - GPIO
Buzzer -    33 - 13
IR LED -    23 - 11
DS18B20 -   07 - 04
SW CNTR -   35 - 19
SW Right -  37 - 26
SW Up -     38 - 20
SW Down -   36 - 16
SW Left -   40 - 21




PCB Versions:
-------------
Version 1.1:
Production Date:  TBA
PCB Details:

Fixed: input ideal diodes don't seem to block the input LEDs from lighting up! Added dual MOSFETs to schematic.  NOTE: PCB layout hasn't been updated.
* have a look at adding a slot in the PCB for the camera flex cable
* have a look at changing DC-DC IC from MPC1584 to SY7088?
* add back silkscreen polarity for HDMI and Flood light outputs +-12V (front is done)
* should the PCB be rotated so that it's not covering the CPU?  Heat issues?
* check PSU capacitors for correct footprint size and voltage.  Might need to be a large size?
* implement and check 1.0a changes


Version 1.0a:
Production Date:  Manual changes to 1.0 PCB
PCB Details:  

Done: Updated Schematic and PCB layout with small fixes/values for PSU.  No PCB made.
Done: changed R18 value from 39K to 36K.  Sets the output voltage to 5.1V
Done: changed R20 from 210K to 200K - what I had on hand
Done: added R21 to help shutdown PSU via enable pin.
Done: successfully tested PSU U8 with 2A version IC:  MP1582
Done: rotate D3 silkscreen diode symbol.  Was wrong way around!  Add a + to make sure.
Done: Fix various silkscreen text:  J8:  add front side text, Add + symbol to tantalum caps,
Done: change R15 from 0805 to 0603.
Done: Add a + to front PCB for buzzer alignment.
Done: Add GPIO text info to back of PCB


Version 1.0:
Production Date:  2020-11-07
PCB Details:  JLCPCB, 1.6mm, white, 5 PCB's, A$17.

Initial version of the PCB.  



PCB Project Journal:
--------------------
2023-02-14:  updated designs to KiCAD v6.x and added dual MOSFETs to ideal diodes
2020-12-26:  Made up another PCB with correct/good PSU IC U8.
2020-12-24:  Tweeked PSU design feedback resistor values for 5.1V output.  Updated schematic to V1.0a
2020-12-22:  Successfully Tested PCB with a MP1582 (2A output only).  PSU design is ok.  
2020-12-20:  found an old MP1584 IC U8.  Bad batch of IC's, blew all IC's at 12V pass through!!
2020-12-07:  assembled the V1.0 PCB and tested ok.  Waiting on the U8 to arrive from China.
2020-12-01:  V1.0 PCB arrived back from JLCPCB
2020-11-07:  sent off V1.0 PCB for manufacture at JLCPCB


































